const mongoose = require('mongoose');

var noteSchema = mongoose.Schema({
    notebookId: String,
    userId: String,
    title: String,
    body: String,
    in_trash: { type: Boolean, default: false },
    createdAt: { type: Date, default: new Date },
    updatedAt: { type: Date, default: new Date },
});

noteSchema.method('update', function (updates, cb) {
    Object.assign(this, updates, { updatedAt: new Date });
    this.parent().save(cb);
});

var Note = mongoose.model('Note', noteSchema);

module.exports = Note;