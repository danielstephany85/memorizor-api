const mongoose = require('mongoose');

const notebookSchema = new mongoose.Schema({
    title: { type: String, required: true },
    description: String,
    in_trash: { type: Boolean, default: false },
    createdAt: { type: Date, default: new Date },
    updatedAt: { type: Date, default: new Date },
});

module.exports = notebookSchema;