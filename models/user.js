const mongoose = require('mongoose');
const notebookSchema = require('./notebook');

const userSchema = mongoose.Schema({
    userId: String,
    name: String,
    email: { type: String, unique: true, required: true, trim: true },
    password: { type: String, required: true},
    createdAt: { type: Date, default: new Date },
    updatedAt: { type: Date, default: new Date },
    notebooks: [notebookSchema]
});

const User = mongoose.model('User', userSchema);

module.exports = User;