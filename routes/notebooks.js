var express = require('express');
var router = express.Router();
const validateToken = require('../middleware/index').validateToken;
const Note = require('../models/note');

// add a new notebook
router.post('/', validateToken, function (req, res, next) {
  if (!req.body.title) {
    res.status(400);
    return res.json({
      status: "error",
      data: { message: "title is required" }
    });
  }

  function hasDuplicate() {
    for(let book of req.user.notebooks) {
        if(book.title.toLowerCase() === req.body.title) {
          return true;
        }
    }
    return false;
  }

  if (hasDuplicate()){
    res.status(400);
    return res.json({
      status: "error",
      data: { message: `notebook with title "${req.body.title}" already exists` }
    });
  }

  req.user.notebooks.push({ title: req.body.title });
  req.user.save((err, user) => {
    if (err) return next(err);
    res.status(200)
    return res.json({
      status: "success",
      data: { user: user }
    });
  });
  
});

//delete notebook data
router.post('/trash', validateToken, function (req, res, next) {
  if(req.body.notebookId){

    const newNotebookList = req.user.notebooks.map(book => {
      if (typeof book._id !== "undefined"){
        if (String(book._id) === String(req.body.notebookId)) {
          book.in_trash = true;
          book.updatedAt = new Date();
        } 
      }
      return book;
    });

    req.user.newNotebookList = newNotebookList;

    //update notebook list
    req.user.save((err, user) => {
      if (err) return next(err);
      //update notes
      Note.updateMany({ notebookId: req.body.notebookId }, { $set: { in_trash: true, updatedAt: new Date() } })
        .exec(function (err2, doc) {
          if (err) return next(err);
          if (doc.ok) {
            //update notebook list on user
            req.user.notebooks = newNotebookList;
            req.user.save();
            //return update success
            return res.json({
              status: "success",
              data: { user: user }
            });
          };
        }); 
    });

  } else {
    return res.json({
      status: "error",
      data: { message: "notebook id is required" }
    });
  }
});


module.exports = router;