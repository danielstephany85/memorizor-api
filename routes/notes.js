const express = require('express');
const router = express.Router();
const validateToken = require('../middleware/index').validateToken;
const Note = require('../models/note');


// create a new note
router.post('/', validateToken, function (req, res, next) {
  if (typeof req.body.body !== "undefined" || typeof req.body.title !== "undefined") {
    let newNote = {
      title: req.body.title || '',
      body: req.body.body || '',
      notebookId: req.body.notebookId || '',
      userId: req.user._id
    }
    Note.create(newNote, function(err, note){
      if(err) return next(err);
      return res.json({
        status: 'success',
        data: {
          note: note
        }
      });
    });
  }else {
    return res.json({
      status: "error",
      data: {
        message: `the note could not be saved`,
      }
    });
  }
});

// get all notes for this user
router.get('/', validateToken, function (req, res, next) {
  Note.find({ userId: req.user._id, in_trash: false})
    .exec((err, doc) => {
      if (err) { return next(err); }
      if (!doc) { return res.status(403).json({ status: "error", data: { message: "Item not found" } }); }
      return res.json({
        status: 'success',
        data: {
          notes: doc
        }
      });
    });
});

//get single note
router.get('/:noteId', validateToken, function (req, res, next) {
  console.log(req.params.noteId);
  Note.findById(req.params.noteId)
    .exec((err, doc) => {
      if (err) { return next(err); }
      if (!doc) { return res.status(403).json({ status: "fail", data: { message: "Item not found" } }); }
      return res.json({
        status: 'success',
        data: {
          note: doc
        }
      });
    });
});

// get all notes for a notebook for this user
router.get('/notebook-notes/:notebookId', validateToken, function (req, res, next) {
  if (req.params.notebookId){
    Note.find({ userId: req.user._id, notebookId: req.params.notebookId, in_trash: false})
      .exec((err, doc) => {
        if (err) { return next(err); }
        if (!doc) { return res.status(403).json({ status: "fail", data: { message: "Item not found" } }); }
        console.log(doc);
        return res.json({
          status: 'success',
          data: {
            notes: doc
          }
        });
      });
  }else {
    return res.json({ status: "fail", data: { message: "notebookeId is required" } });
  }
});

//delete one note
router.delete('/:noteId', validateToken, function (req, res, next) {
  Note.remove({ _id: req.params.noteId })
  .exec((err, doc)=> {
    if(err){ return next(err);}
    if (!doc) { return res.status(403).json({ status: "fail", data: { message: "Item not found" } }); }
    return res.json({
      status: 'success',
      data: {
        ...doc
      }
    });
  });
});

//delete a set of notes
router.delete('/', validateToken, function (req, res, next) {
  if (req.body.notes) {
    Note.remove({ _id: { $in: req.body.notes } })
      .exec((err, doc) => {
        if (err) { return next(err); }
        if (!doc) { return res.status(403).json({ status: "fail", data: { message: "Item not found" } }); }
        return res.json({
          status: 'success',
          data: {
            ...doc
          }
        });
      });
  } else {
    return res.status(403).json({ status: "error", data: { message: "no notes where provided" } });
  }
});

router.put('/restore-notes', validateToken, function (req, res, next) {
  if (req.body.notes, req.body.notebookId){
    Note.updateMany({ _id: { $in: req.body.notes } }, { $set: { in_trash: false, notebookId: req.body.notebookId } })
      .exec((err, doc) => {
        if (err) { return next(err); }
        if (!doc) { return res.status(403).json({ status: "error", data: { message: "Item not found" } }); }
        return res.json({
          status: 'success',
          data: {
            notes: doc
          }
        });
      });
  }else {
    return res.status(403).json({ 
      status: "error", 
      data: { message: "ERROR: \"/notes/restore-notes\" requires \"notes\" and \"notebookId\" arguments." } 
    });
  }
  
});

router.put('/trash-note/:noteId', validateToken, function (req, res, next) {
  if (req.params.noteId) {
    Note.updateOne({ _id: req.params.noteId }, { $set: { in_trash: true } }, { new: true })
      .exec((err, doc) => {
        if (err) { return next(err); }
        if (!doc) { return res.status(403).json({ status: "error", data: { message: "Item not found" } }); }
        return res.json({
          status: 'success',
          data: {
            notes: doc
          }
        });
      });
  } else {
    return res.status(403).json({
      status: "error",
      data: { message: "ERROR: \"/notes/trash-note/:noteId\" requires \"noteId\" param." }
    });
  }
});

router.put('/trash-many-notes', validateToken, function (req, res, next) {
  if (req.body.notes) {
    Note.updateMany({ _id: { $in: req.body.notes } }, { $set: { in_trash: true } })
      .exec((err, doc) => {
        if (err) { return next(err); }
        if (!doc) { return res.status(403).json({ status: "error", data: { message: "Item not found" } }); }
        return res.json({
          status: 'success',
          data: {
            notes: doc
          }
        });
      });
  } else {
    return res.status(403).json({
      status: "error",
      data: { message: "ERROR: \"/notes/trash-many-notes\" requires \"notes\"" }
    });
  }

});

router.put('/change-notebook', validateToken, function (req, res, next) {
  if (req.body.notes, req.body.notebookId) {
    Note.updateMany({ _id: { $in: req.body.notes } }, { $set: {notebookId: req.body.notebookId } })
      .exec((err, doc) => {
        if (err) { return next(err); }
        if (!doc) { return res.status(403).json({ status: "error", data: { message: "Item not found" } }); }
        return res.json({
          status: 'success',
          data: {
            notes: doc
          }
        });
      });
  } else {
    return res.status(403).json({
      status: "error",
      data: { message: "ERROR: \"/notes/restore-notes\" requires \"notes\" and \"notebookId\" arguments." }
    });
  }

});

// update a existing note
router.put('/update-note/:noteId', validateToken, function (req, res, next) {
  Note.update({ _id: req.params.noteId },{
      ...req.body.note
    })
    .exec((err, doc) => {
      if (err) { return next(err); }
      if (!doc) { return res.status(403).json({ status: "error", data: { message: "Item not found" } }); }
      return res.json({
        status: 'success',
        data: {
          notes: doc
        }
      });
    });
});



module.exports = router;
