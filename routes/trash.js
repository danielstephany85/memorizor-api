const express = require('express');
const router = express.Router();
const validateToken = require('../middleware/index').validateToken;
const Note = require('../models/note');

// get all notes for 
router.get('/', validateToken, function (req, res, next) {
    Note.find({ userId: req.user._id, in_trash: true })
        .exec((err, doc) => {
            if (err) { return next(err); }
            if (!doc) { return res.status(403).json({ status: "error", data: { message: "Item not found" } }); }
            return res.json({
                status: 'success',
                data: {
                    notes: doc
                }
            });
        });
});

router.put('/restore-notebook', validateToken, function (req, res, next) {
    if (req.body.notebookId) {
        const newNotebookList = req.user.notebooks.map(book => {
            if (typeof book._id !== "undefined") {
                if (String(book._id) === String(req.body.notebookId)) {
                    book.in_trash = false;
                    book.updatedAt = new Date();
                }
            }
            return book;
        });

        req.user.newNotebookList = newNotebookList;

        //update notebook list
        req.user.save((err, user) => {
            if (err) return next(err);
            //update notes
            Note.updateMany({ notebookId: req.body.notebookId }, { $set: { in_trash: false, updatedAt: new Date() } })
                .exec(function (err2, doc) {
                    if (err) return next(err);
                    if (doc.ok) {
                        //return update success
                        return res.json({
                            status: "success",
                            data: { user: user }
                        });
                    };
                });
        });

    } else {
        return res.json({
            status: "error",
            data: { message: "notebook id is required" }
        });
    }
});


router.put("/delete-notebook", validateToken, function(req, res, next) {
    if (req.body.notebookId) {
    
        const newNotebookList = req.user.notebooks.filter(book => (String(book._id) !== String(req.body.notebookId)));

        req.user.notebooks = newNotebookList;

        //update notebook list
        req.user.save((userCollectionError, user) => {
            if (userCollectionError) return next(err);
            //update notes
            Note.deleteMany({ notebookId: req.body.notebookId })
                .exec(function (noteCollectionError, doc) {
                    if (noteCollectionError) return next(err);
                    if (doc.ok) {
                        return res.json({
                            status: "success",
                            data: { 
                                userData: user,
                                noteData: doc
                             }
                        });
                    };
                });
        });
    } else {
        return res.status(403).json({ status: "error", data: { message: '"delete-notebook:" expects notebookId' } });
    }
});


// delete all notes that have in_trash true for more than 30 Days
router.delete('/remove-outdated', validateToken, function (req, res, next) {

    // getting a day from 30 days ago 
    let thirtyDaysAgo = (Date.now() - 1000 * 60 * 60 * 24 * 30);
    thirtyDaysAgo = new Date(thirtyDaysAgo).toISOString();
    
    let removeNotes = function () {
        return new Promise((resolve, reject)=>{
            Note.remove({ userId: req.user._id, in_trash: true, updatedAt: { $lt: thirtyDaysAgo } })
                .exec((err, doc) => {
                    if (err) { return reject(err); }
                    if (!doc) { return reject("ERROR: removeNotes found no notes"); }
                    return resolve(doc);
                });
        });
    }
    
    let removeNotebooks = function (notesResponse) {
        return new Promise((resolve, reject)=> {
            if (req.user) {
                const newNotebookList = req.user.notebooks.filter(notebook => {
                    if (!notebook.in_trash) return notebook; 
                    if (notebook.in_trash && Date.parse(notebook.updatedAt) > Date.parse(thirtyDaysAgo)){
                        return notebook
                    }
                });
                req.user.notebooks = newNotebookList;
                req.user.save((err, user) => {
                    if (err) return reject(err);
                    return resolve({
                        status: "success",
                        data: { 
                            user: user,
                             note: notesResponse
                            }
                    });
                });
            } else {
                return reject('ERROR: removeNotebooks expects "req.user"');
            }
        });
    }

    removeNotes()
    .then(removeNotebooks)
    .then(json => {
        return res.json(json); 
    })
    .catch((e)=>{
        return next(e)
    });    
});

module.exports = router;