const express = require('express');
const router = express.Router();
const validateToken = require('../middleware/index').validateToken;
const User = require('../models/user');
const bcrypt = require('bcrypt');

router.get('/', validateToken, (req, res, next) => {
    res.json({
        status: "success",
        data: {
            user: req.user
        }
    });
});

router.put('/', validateToken, (req, res, next) => {
    if (req.body.name && req.body.email) {
        let updateProps = {
            name: req.body.name,
            email: req.body.email
        };

        User.findByIdAndUpdate(req.user._id, { $set: updateProps })
            .exec((err, doc) => {
                if (err) { return next(err); }
                if (!doc) { 
                    return res.status(403).json({ status: "error", data: { message: "Item not found" } }); 
                }
                console.log(doc);
                delete doc._doc.password;
                delete doc._doc.__v;
                return res.json({
                    status: 'success',
                    data: {
                        user: doc
                    }
                });
            });
    } else {
        return res.status(403).json({
            status: "error",
            data: { message: 'the "name" and "email" values are required' }
        });
    }
});

router.put('/update-password', validateToken, function(req, res, next){
    const { currentPass, newPass, newPass2} = req.body;
    let hasCorrectPass = false;

    if (currentPass && newPass && newPass2){
        if(newPass === newPass2){
            console.log('passwords are equal');
        }

        if (currentPass){
            bcrypt.compare(currentPass, req.user._doc.password, function (err, isvalid) {
                if(err){
                    next(err);
                }
                if (isvalid) {
                    bcrypt.hash(newPass, 10, (err, hash) => { 
                        User.findByIdAndUpdate(req.user._id, { $set: { password: hash} })
                            .exec((err, doc) => {
                                if (err) { return next(err); }
                                if (!doc) {
                                    return res.status(403).json({ status: "error", data: { message: "Item not found" } });
                                }
                                return res.json({
                                    status: "success",
                                    data: { message: 'password updated' }
                                });
                            });
                    });
                } else {
                    return res.status(403).json({
                        status: "error",
                        data: { message: 'incorrect current password' }
                    });
                }
            });
        }

    } else {
        return res.status(403).json({
            status: "error",
            data: { message: 'the "newPass", "newPass2" and "currentPass" values are required' }
        });
    }
});

module.exports = router;