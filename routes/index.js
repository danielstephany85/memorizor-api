const express = require('express');
const router = express.Router();
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const config = require('../config');
const User = require('../models/user');



/* GET home page. */
router.post('/create-user', function(req, res, next) {
  if(req.body.name && req.body.email && req.body.password){
    User.findOne({email: req.body.email})
      .exec(function (error, user){
      if(error) return next(error);
      if(user){
        return res.json({
          status: "error",
          data: {
            message: `user with email: ${req.body.email} already exists.`,
          }
        });
      }
      else {
        let newUser = {
          name: req.body.name,
          email: req.body.email,
          password: req.body.password
        }
        bcrypt.hash(newUser.password, 10, (err, hash)=>{
          if(err){
            return next(err);
          }
          newUser.password = hash;
          User.create(newUser, function(err, user){
            if(err){
               return next(err);
            }else {
              let payload = {id: user._id};
              var token = jwt.sign(payload, config.secret);
              //remove password from returned data
              delete user._doc.password;
              delete user._doc.__v;
              return res.json({
                status: "success",
                data: {
                  user: user,
                  token: token
                }
              });
            }
          });
        });
      }
    });
  }else {
    return res.json({
      status: "error",
      data: {
        message: `name, email and password are required`,
      }
    });
  }
});


router.post('/authenticate-user', function (req, res, next) {
  if(req.body.email && req.body.password){
    User.findOne({email: req.body.email})
    .exec(function(err, user){
      if(!user){
        return res.json({
          status: "error",
          data: {
            message: `no user matching ${req.body.email} was found`,
          }
        });
      }else {
         bcrypt.compare(req.body.password, user.password, (err, result)=>{
           if (result) {
             const payload = { id: user._id };
             var token = jwt.sign(payload, config.secret);
             //remove password from returned data
             delete user._doc.password;
             delete user._doc.__v;
             return res.json({
               status: "success",
               data: {
                 user: user,
                 token: token
               }
             });
           } else {
             return res.json({
               status: "error",
               data: {
                 message: "incorrect password"
               }
             });
           }
         });
      }
    });
  }else {
    return res.json({
      status: "error",
      data: {
        message: `email and password are required`,
      }
    });
  }

});


module.exports = router;
