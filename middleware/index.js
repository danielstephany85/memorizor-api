const jwt = require('jsonwebtoken'); // used to create, sign, and verify tokens
const config = require('../config');
const User = require('../models/user');

const validateToken = function (req, res, next) {
    const token = req.body.token || req.query.token || req.headers['x-access-token'];
    if (token) {
        jwt.verify(token, config.secret, function (err, decoded) {
            if (err) {
                return res.json({ success: false, data: { message: 'Failed to authenticate token.' } });
            } else {
                // get the user using the id encoded into the token
                User.findById(decoded.id)
                .exec((err, doc) => {
                    if (err) { return next(err); }
                    if (!doc) { return res.status(403).json({ status: "fail", data: { message: "Item not found" } }); }
                    req.user = doc;
                    return next(); // token is valid
                });
            }
        });
    } else {
        return res.status(403).json({
            success: false,
            message: 'No token provided.'
        });
    }
}

module.exports.validateToken = validateToken;