module.exports = {
  apps : [{
    name: "memorizor-api",
    script: "./bin/www",
  }],
  deploy : {
    production : {
      "env": {
        "NODE_ENV": "production"
      },
      user: 'bitnami',
      host: '52.25.69.116',
      key: "/Users/Daniel/private_keys/danielsHouse.pem",
//      key: "/opt/atlassian/pipelines/agent/data/id_rsa",
      ref  : 'origin/master',
      repo: 'git@bitbucket.org:danielstephany85/memorizor-api.git',
      path: '/home/bitnami/projects',
      'pre-deploy-local': '',
      'post-deploy': 'npm install && pm2 reload ecosystem.config.js --env production',
      'pre-setup': ''
    }
  }
};
