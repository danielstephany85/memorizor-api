require('dotenv').config();

module.exports = {
    secret: process.env.JWT_SECRET,
    dataBase: process.env.DATABASE,
    allowedOrigin: process.env.ALLOWED_ORIGIN
}
